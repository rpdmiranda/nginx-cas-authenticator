package rpmiranda.nginx.auth.cas;


import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class CasAuthenticatorApp {

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		Jaxb2RootElementHttpMessageConverter jaxbMessageConverter = new Jaxb2RootElementHttpMessageConverter();
		List<MediaType> mediaTypes = new ArrayList<>();
		mediaTypes.add(MediaType.TEXT_HTML);
		jaxbMessageConverter.setSupportedMediaTypes(mediaTypes);
		return builder
				.additionalMessageConverters(jaxbMessageConverter)
				.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(CasAuthenticatorApp.class, args);
	}
}
