package rpmiranda.nginx.auth.cas;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import rpmiranda.nginx.auth.cas.response.AuthenticationFailureType;
import rpmiranda.nginx.auth.cas.response.AuthenticationSuccessType;
import rpmiranda.nginx.auth.cas.response.ServiceResponseType;

/**
 * Autenticação pelo nginx
 */
@Controller
public class NginxAuthenticator {

	public static final String CAS_AUTH_NGINX_COOKIE_NAME = "CAS_AUTH_NGINX";
	/**
	 * Cookies per CAS ticket, so we can invalidate the sessions
	 */
	private final ConcurrentHashMap<String, String> cookiePerCasTicket = new ConcurrentHashMap<>();

	/**
	 * Authentication per session (using a local cookie)
	 */
	private final Cache<String, AuthenticationData> authenticationDataPerSession = CacheBuilder.newBuilder()
			.expireAfterAccess(Duration.ofMinutes(5))
			.removalListener((RemovalListener<String, AuthenticationData>) notification -> cookiePerCasTicket.remove(notification.getValue().getTicket()))
			.build();

	private final Logger LOGGER = LoggerFactory.getLogger(NginxAuthenticator.class);

	@Autowired
	private RestTemplate restTemplate;

	@RequestMapping(path = "/check", method = RequestMethod.GET)
	@ResponseBody
	public void check(HttpServletRequest request, HttpServletResponse response) throws ExecutionException {
		Cookie cookie = Optional.ofNullable(request.getCookies())
				.map(Stream::of)
				.orElseGet(Stream::empty)
				.filter(c -> "CAS_AUTH_NGINX".equals(c.getName()))
				.findFirst()
				.orElseGet(() -> {
					Cookie newCookie = new Cookie(CAS_AUTH_NGINX_COOKIE_NAME, UUID.randomUUID().toString());
					newCookie.setMaxAge(-1);
					newCookie.setSecure(true);
					newCookie.setHttpOnly(true);
					newCookie.setPath("/");
					response.addCookie(newCookie);
					return newCookie;
				});

		String uri = Optional.ofNullable(request.getHeader("X-Nginx-Authenticate-Uri")).orElse("/");
		LOGGER.debug("URL: {}", uri);

		// Find the current session, or create a new one if there is none or if the the old one has expired
		AuthenticationData auth = authenticationDataPerSession.get(cookie.getValue(), () -> new AuthenticationData(uri));

		if (!auth.isAuthenticated()) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
		} else {
			response.setStatus(HttpStatus.OK.value());
		}
	}

	@RequestMapping(path = "/auth", method = RequestMethod.POST)
	@ResponseBody
	public void logout(HttpServletRequest request, HttpServletResponse response) {
		System.out.println(request);
		response.setStatus(HttpStatus.OK.value());
	}

	@RequestMapping(path = "/auth", method = RequestMethod.GET)
	@ResponseBody
	public void authenticate(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String ticket = request.getParameter("ticket");

		Optional<Cookie> cookie = Optional.ofNullable(request.getCookies())
				.map(Stream::of)
				.orElseGet(Stream::empty)
				.filter(c -> CAS_AUTH_NGINX_COOKIE_NAME.equals(c.getName()))
				.findFirst();

		AuthenticationData auth = cookie
				.map(Cookie::getValue)
				.map(key -> authenticationDataPerSession.getIfPresent(key))
				.orElse(null);

		LOGGER.debug("Ticket {}; cookie {}, auth {}", ticket, cookie, auth);
		if (ticket == null || auth == null) {
			response.sendRedirect("/");
			return;
		}

		// If the user was authenticated before, invalidate the old data and redirect to the root URL, so the user
		// can reauthenticate
		if (auth.isAuthenticated()) {
			authenticationDataPerSession.invalidate(cookie.get().getValue());
			response.sendRedirect("/");
			return;
		}

		String casUrl = request.getHeader("X-Nginx-Authenticate-CasUrl");
		String casService = request.getHeader("X-Nginx-Authenticate-CasService");
		if (!StringUtils.hasText(casUrl) || !StringUtils.hasText(casService)) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			return;
		}
		URI uri = UriComponentsBuilder.fromHttpUrl(casUrl)
				.pathSegment("serviceValidate")
				.queryParam("service", casService)
				.queryParam("ticket", ticket)
				.build()
				.toUri();

		ResponseEntity<ServiceResponseType> casResponseEntity = restTemplate.getForEntity(uri, ServiceResponseType.class);

		ServiceResponseType casResponse = casResponseEntity.getBody();
		String login = Optional.of(casResponse)
				.map(ServiceResponseType::getAuthenticationSuccess)
				.map(AuthenticationSuccessType::getUser)
				.orElse(null);
		if (login != null) {
			auth.updateAuthenticationData(login, ticket);
			cookiePerCasTicket.put(ticket, cookie.get().getValue());
			response.sendRedirect(auth.getFirstUri());
		} else {
			AuthenticationFailureType casFailure = casResponse.getAuthenticationFailure();
			if (casFailure != null) {
				LOGGER.error("Error during CAS validation: HttpStatus {} Code {}: {}", casResponseEntity.getStatusCode(), casFailure.getCode(), casFailure.getValue());
				response.sendError(HttpStatus.FORBIDDEN.value());
			} else {
				LOGGER.error("Error during CAS validation: HttpStatus {}", casResponseEntity.getStatusCode());
				response.sendError(HttpStatus.FORBIDDEN.value());
			}
		}
	}

}
