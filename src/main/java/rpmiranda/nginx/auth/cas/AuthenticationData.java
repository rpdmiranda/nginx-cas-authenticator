package rpmiranda.nginx.auth.cas;

/**
 * Authentication data
 */
public class AuthenticationData {

	public AuthenticationData(String firstUri) {
		this.firstUri = firstUri;
	}

	private final String firstUri;
	private String username;
	private String ticket;


	public boolean isAuthenticated() {
		return (username != null);
	}

	public void updateAuthenticationData(String username, String ticket) {
		this.username = username;
		this.ticket = ticket;
	}

	public String getUsername() {
		return username;
	}

	public String getFirstUri() {
		return firstUri;
	}

	public String getTicket() {
		return ticket;
	}

	@Override
	public String toString() {
		return "AuthenticationData{" +
				", firstUri='" + firstUri + '\'' +
				", username='" + username + '\'' +
				", ticket='" + ticket + '\'' +
				'}';
	}
}
